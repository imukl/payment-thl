package com.alipay.trade.model.hb;

/**
 * Created by liuyangkly on 15/8/27.
 */
public enum Product {
    /**
     * Fp product.
     */
    FP // 当面付产品

    ,
    /**
     * Mp product.
     */
    MP  // 医疗产品
}
